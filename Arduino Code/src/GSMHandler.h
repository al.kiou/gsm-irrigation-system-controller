#ifndef GSMHANDLER
#define GSMHANDLER
#include <Arduino.h>
#include <GPRS_Shield_Arduino.h>
#include <SoftwareSerial.h>

#define MESSAGE_LENGTH

class GSMHandler {
  public:
      void smsCheck();

  private:
      void handler();
      char phone[16];
      char datetime[24];
      char message[MESSAGE_LENGTH];
      int messageIndex;


}



#endif
