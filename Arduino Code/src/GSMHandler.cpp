#include <Arduino.h>
#include <GSMHandler.h>
#include <GPRS_Shield_Arduino.h>
#include <SoftwareSerial.h>

#define PIN_TX    7
#define PIN_RX    8
#define BAUDRATE  9600

GPRS gprs(PIN_TX,PIN_RX,BAUDRATE);//RX,TX,PWR,BaudRate

void GSMHandler :: smsCheck(){
  messageIndex = gprs.isSMSunread();
   if (messageIndex > 0) { //At least, there is one UNREAD SMS
      gprs.readSMS(messageIndex, message, MESSAGE_LENGTH, phone, datetime);
}
